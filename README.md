# hpctls

Tool to verify probabilistic hyper-properties

Requirements:
Please install the following python packages
pip3 install numpy
pip3 install scipy
pip3 install lark-parser

To run the case studies:
cd <this folder>
export PYTHONPATH=<this folder>/src
python3 examples/gabfeed/vulnerability.py 
python3 examples/dining_crypt/dining_crypt.py
python3 examples/prob_nonint/prob_nonint.py 
python3 examples/cache/cache.py 
(The results are output to the terminal)
