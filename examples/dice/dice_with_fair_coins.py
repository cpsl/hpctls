from parser import *
from formula import *
from model_checker import *
from lark import Lark,Transformer

start, model = json_to_dtmc('examples/dice/dice_with_fair_coins.json')
formula = txt_to_formula('examples/dice/test2.prop')
#print(formula)
hpctls = HPCTLS(model)
hpctls.set_params(formula, 0.05, 0.01)
print(hpctls.verify(formula, [start, start]))
