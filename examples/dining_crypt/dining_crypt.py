import matplotlib.pyplot as plt 
from parser import *
from formula import *
from model_checker import *
from lark import Lark,Transformer
import random
import matplotlib.pyplot as plt 
import numpy as np 

class DC(Model):
    def __init__(self, N):
        self.N = N
    def rand_path_bkp(self, s, n):
        # Dining cryptographers protocol
        secret_keys = np.random.randint(low=0, high=2, size=(self.N,self.N))
        for i in range(self.N):
            secret_keys[i][i] = 0
            for j in range(i,self.N):
                secret_keys[i][j] = secret_keys[j][i] 
        #print(secret_keys)
        xor = lambda x : (np.sum(x) % 2)
        nxor = lambda x: (np.sum(x) % 2)+1
        if s[:1] == 'c':
            ann = [nxor (secret_keys[x][:]) for x in range(self.N)]
        if s == 'nsa':
            ann = [xor (secret_keys[x][:]) for x in range(self.N)]
        
        c1 = int(s[1:])
        c2 = np.random.choice(range(self.N))
        shared = secret_keys[c1][c2] 
        s0 = State(s, AP=[s])
        s1 = State(s, AP=[s, 'l'+str(shared)])
        #print(ann)
        #print(xor(ann))
        return Path(states=[s0,s1])
    def rand_path(self, s, n):
        shared_secrets = np.random.randint(low = 0, high = 2, size=self.N)
        c2 = np.random.choice(range(self.N))
        shared = shared_secrets[c2]
        s0 = State(s, AP=[s])
        s1 = State(s, AP=[s, 'l'+str(shared)])
        return Path(states=[s0,s1])

#model = DC(100)
#rpath = model.rand_path('c0', 10)
#for s in rpath.get_states():
#    print(s.labels())
for N in [100, 1000]:
    for eps in [0.05, 0.1, 0.2]:
        for delta in [0.02]:
            model = DC(N)
            formula = txt_to_formula('examples/dining_crypt/dining_crypt.prop')
            #formula.set_eps(eps)
            #formula.set_delta(delta)
            #print(formula)
            hpctls = HPCTLS(model, bound=N)
            hpctls.set_params(formula, eps, delta)
            hpctls.verify(formula, ['c0', 'c1'])