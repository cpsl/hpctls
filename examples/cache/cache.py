import matplotlib.pyplot as plt 
from parser import *
from formula import *
from model_checker import *
from lark import Lark,Transformer
import random
import matplotlib.pyplot as plt 
import numpy as np 

class RandomCache(Model):
    def __init__(self, N, nb):
        self.N = N
        self.b = range(nb)
    def rand_path(self, s, n):
        self.cache = [0 for x in range(self.N)]
        states = []
        for i in range(n+self.N):
            b = int(np.random.normal(len(self.b)/2, len(self.b)*0.01))
            if b in self.cache:
                states.append(State('H', AP='H'))
            else:
                states.append(State('M', AP='M'))
                if 0 in self.cache:
                    loc = self.cache.index(0)
                    self.cache[loc] = b
                else:
                    rand = random.choice(range(self.N))
                    self.cache[rand] = b
        #
        r = random.choice(range(self.N))
        #print(s, [s1.name for s1 in states[r:r+n]])
        return Path(states[self.N:])

#model = RandomCache(3)
#rpath = model.rand_path('s0', 10)
#for s in rpath.get_states():
#    print(s.labels(), end = ' ')
#print()

for N in [10, 20]:
    for eps in [0.05, 0.01]:
        for delta in [0.01, 0.001]:
            model = RandomCache(256,1024)
            formula = txt_to_formula('examples/cache/cache.prop')
            hpctls = HPCTLS(model, bound=N)
            hpctls.set_params(formula, eps, delta)
            hpctls.verify(formula, ['s0', 's1'])