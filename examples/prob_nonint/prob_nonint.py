import matplotlib.pyplot as plt 
from parser import *
from formula import *
from model_checker import *
from lark import Lark,Transformer
import random
import matplotlib.pyplot as plt 
import numpy as np 
class ProbNonInt(Model):
    def __init__(self, N):
        self.N = N
        self.threads = np.zeros(self.N)
        #self.traces1 = [/]
        #self.traces2 = []
        self.traces = []
        self.p1 = []
        self.p2 = []
        for k in range(self.N):
            self.traces.append(k)
            self.p1.append(k)
            #self.p2.append(self.N*k*(k-1)/2.0)
            self.p2.append(2**k)
    def rand_path(self, s, n):
        if s == 'h0':
            t = random.choices(self.traces, weights=self.p1)[0]
        else:
            t = random.choices(self.traces, weights = self.p2)[0]
        s0 = State(s, AP=[s])
        s1 = State(s, AP=[s, 't', 'l'+str(t%2)])
        return Path([s0,s1])
#model = ProbNonInt(10)
#rpath = model.rand_path("h0", 3)
#for s in rpath.get_states():
#    print(s.labels())
for N in [20, 50, 100]:
    for eps in [0.0]:
        for delta in [0.01, 0.001]:
            model = ProbNonInt(N)
            formula = txt_to_formula('examples/prob_nonint/prob_nonint.prop')
            #formula.set_eps(eps)
            #formula.set_delta(delta)
            #print(formula)
            hpctls = HPCTLS(model, bound=N)
            hpctls.set_params(formula, eps, delta)
            hpctls.verify(formula, ['h0', 'h1'])
