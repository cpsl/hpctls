import matplotlib.pyplot as plt
import random

with open('examples/gabfeed/times1.txt', 'r') as fp:
    times1 = fp.read().splitlines()
with open('examples/gabfeed/times2.txt', 'r') as fp:
    times2 = fp.read().splitlines()

times1 = [int(float(x)/1000) for x in times1]
times2 = [int(float(x)/1000) for x in times2]
random.shuffle(times1)
random.shuffle(times2)

plt.figure()
plt.scatter(range(len(times1)), times1, marker='x')
plt.scatter(range(len(times2)), times2, marker='x')
plt.legend(['k1', 'k2'])
plt.show()

plt.figure()
plt.hist(times1, alpha=0.5, cumulative=True, density=True)
plt.hist(times2, alpha=0.5,cumulative=True, density=True)
plt.legend(['k1', 'k2'])
plt.show()
