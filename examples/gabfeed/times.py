import subprocess

public_keys = []

with open('examples/gabfeed/keys.txt','r') as fp:
    public_keys = fp.read().splitlines()

times = []
for i in range(len(public_keys)):
    out = subprocess.Popen(['java', '-classpath', 'examples/gabfeed/gabfeed.jar' ,'com.cyberpointllc.stac.auth.KeyExchangeVerifier', '0xd873d624adf98413', public_keys[i]], 
           stdout=subprocess.PIPE, 
           stderr=subprocess.STDOUT)
    stdout, stderr = out.communicate()
    times.append(float(stdout.split()[0]))
    print(i,times[i])

with open('examples/gabfeed/k1times.txt', 'w') as fp:
    for t in times:
        fp.write('%s\n' % t)

