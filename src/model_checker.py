from formula import *
from model import *
import numpy as np
from scipy.stats import bernoulli
from scipy.optimize import minimize
import math
import random

import warnings
warnings.filterwarnings("ignore")

import time
class HPCTLS:
    def __init__(self, model, bound=11):
        self.bound = bound
        self.model = model
        self.alpha_FN = 1e-1
        self.alpha_FP = 1e-1
        self.max_iter = 2e4
        self.pvars = []
        self.N = 0
        self.p1 = 0
        self.p2 = 0

    def set_params(self, f, eps, delta):
        #print(f)
        f.set_eps(eps)
        f.set_delta(delta)
        for f_ in f.get_subformulae():
            if isinstance(f_, ProbLT):
                eps = eps
            self.set_params(f_, eps, delta)

    def get_path_vars(self, f: Formula):
        self.pvars.extend(f.get_path_vars())
        for f_ in f.get_subformulae():
            self.get_path_vars(f_)
        return 

    def satisfies(self, f: Formula, s, paths, n_paths):
        if isinstance(f, Top):
            return True
        if isinstance(f, Atomic):
            if paths[f.get_path_vars()[0]].length() == 0:
                return False
            if f.get_label() in paths[f.get_path_vars()[0]].start_state().labels():
                return True
            else:
                return False
        if isinstance(f, Not):
            return (not self.satisfies(f.get_subformulae()[0], s, paths, n_paths))
        if isinstance(f, Next):
            return self.satisfies(f.get_subformulae()[0], s, [path.suffix(1) for path in paths], n_paths)
        if isinstance(f, And):
            return (self.satisfies(f.get_subformulae()[0], s, paths, n_paths) and
                    self.satisfies(f.get_subformulae()[1], s, paths, n_paths))
        if isinstance(f, Until):
            i = 0
            while(i < self.bound - 1):
                left = self.satisfies(f.get_subformulae()[0], s, [
                                      path.prefix(i-1) for path in paths], n_paths)
                right = self.satisfies(f.get_subformulae()[1], s, [
                                       path.suffix(i) for path in paths], n_paths)
                if left and right:
                    return True
                i += 1
            return False
        if isinstance(f, ForAll):
            states = self.model.states()
            k = len(f.get_path_vars())
            n = len(states)
            self.set_params(f, eps=f.get_eps()/n, delta=f.get_delta())
            for i in range(len(states)-k+1):
                s = [states[i]]
                for j in range(i+1,i+k):
                    s.append(states[j])
                if not self.satisfies(f.get_subformulae()[0], s, paths, n_paths ):
                    return False
            return True

        if isinstance(f, ProbLT):
            i = 0
            T1 = 0
            T2 = 0
            N = 0
            p1,p2,q1,q2,r1,r2,ll_q,ll_r=0,0,0,0,0,0,0,0
            while(i < self.max_iter):
                #if(i % 1 == 0):
                #    print(T1, T2, N, p1, p2, q1, q2, r1, r2, q1-r1, q2-r2,ll_r-ll_q, np.log((1-self.alpha_FP)/self.alpha_FN),f.get_eps(), f.get_delta())
                i += 1
                N += 1
                paths = []
                for k in range(n_paths):
                    self.N += 1
                    paths.append(self.model.rand_path(s[k], self.bound))
                #for path in paths:
                #    print([s.labels() for s in path.get_states()])
                T1  += self.satisfies(f.get_subformulae()[0].get_subformulae()[0], s, paths, n_paths)
                T2  += self.satisfies(f.get_subformulae()[1].get_subformulae()[0], s, paths, n_paths)
                if N <= 100:
                    continue
                p1,p2 = T1*1.0/N, T2*1.0/N
                #p2 += f.get_eps()
                q1, q2, ll_q = self.get_q(p1, p2, T1, T2, N, f.get_eps(), f.get_delta())
                r1, r2, ll_r = self.get_r(p1, p2, q1, q2, T1, T2, N, f.get_eps(), f.get_delta())
                
                if (p1 < (p2+f.get_eps()-f.get_delta())) and ((ll_r- ll_q) > np.log((1-self.alpha_FN)/self.alpha_FP)):
                    return True
                if (p2+f.get_eps() < (p1-f.get_delta())) and ((ll_r - ll_q) > np.log((1-self.alpha_FP)/self.alpha_FN)):
                    return False

                self.p1 = p1
                self.p2 = p2
            return False

    def ll(self, x):
        res = x[2]*np.log(x[0]) + (x[4]-x[2])*np.log(1-x[0]) + x[3]*np.log(x[1])+(x[4]-x[3]) * np.log(1-x[1])       
        return res

    def nll(self, x):
        return -self.ll(x)

    def get_q(self, p1, p2, T1, T2, N, eps, delta):
        cons_p1_lt_p2 = (
            {
                'type': 'eq',
                'fun': lambda x: np.array(x[2]-T1)
            },
            {
                'type': 'eq',
                'fun': lambda x: np.array(x[3]-T2)
            },
            {
                'type': 'eq',
                'fun': lambda x: np.array(x[4]-N)
            },{
            'type': 'eq',
            'fun': lambda x: np.array(x[0]-x[1]-eps-delta)
            }
        )
        cons_p2_lt_p1 = (
            {
                'type': 'eq',
                'fun': lambda x: np.array(x[2]-T1)
            },
            {
                'type': 'eq',
                'fun': lambda x: np.array(x[3]-T2)
            },
            {
                'type': 'eq',
                'fun': lambda x: np.array(x[4]-N)
            },{
            'type': 'eq',
            'fun': lambda x: np.array(x[0]-x[1]-eps+delta)
            }
        ) 
        x0 = [p1, p2, T1, T2, N]
        if p1 < p2+eps-delta:
            ux = minimize(self.nll, x0, constraints=cons_p1_lt_p2)
        else:
            ux = minimize(self.nll, x0, constraints=cons_p2_lt_p1)
        q1 = ux.x[0]
        q2 = ux.x[1]
        return (q1, q2, self.ll([q1, q2, T1, T2, N]))
    
    def get_r(self, p1, p2, q1, q2, T1, T2, N, eps, delta):
        if p1 < p2+eps-delta:
            r1 = q1*((q1-2)*q2 - q1*(delta-eps)+ q2*q2+(delta-eps))/(q1*q1-q1+q2*(q2-1))
            r2 = q2*(q1*q1+q1*(q2-2)+(q2-1)*(delta-eps))/(q1*q1-q1+q2*(q2-1))    
        else:
            r1 = q1*((q1-2)*q2 + (q1-1)*(delta+eps)+ q2*q2)/(q1*q1-q1+q2*(q2-1))
            r2 = q2*(q1*q1+q1*(q2-2)-q2*(delta+eps)+(delta+eps))/(q1*q1-q1+q2*(q2-1))   

        return (r1, r2, self.ll([r1, r2, T1, T2, N]))

    def verify(self, f: Formula, s=['s0']):
        rFile = open("RESULT", 'a+')
        self.get_path_vars(f)
        n_paths = np.max(np.array(self.pvars)) + 1
        paths = []
        N = 0
        t = 0.0
        trues = 0
        for i in range(100):
            self.N = 0
            start = time.time()
            res =  self.satisfies(f, s, paths, n_paths)
            end = time.time()
            if res:
                trues +=1
            N += self.N
            t += (end-start)
            rFile.write("Remove %f %f %d %d %d %f\n"%(self.p1, self.p2, i,int(res),self.N, t/(i+1)))
            rFile.flush()
            #print(i, res, self.N)
        print(" tau = "+ str(self.bound)+", eps = "+ str(f.get_eps())+ ", delta = "+str(f.get_delta())+", Acc. =" + str(1- trues/100.0) +", N = "+ str(N/100) +", Time = "+str(t/100))
        rFile.write("T = %d, eps = %f, delta = %f, Trues = %f, N = %d, Time = %f"%(self.bound, f.get_eps(), f.get_delta(), 1-trues/100.0, N/100, t/100.0))
        rFile.close()
        return res
