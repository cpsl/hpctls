import collections
from numpy.random import choice

class State:
    def __init__(self, name, AP = []):
        self.name = name
        self.AP = AP
    def labels(self):
        return self.AP
    def append_label(self, a):
        self.AP.append(a)
    def to_str(self):
        return self.name

class Path:
    def __init__(self, states=[]):
        self.path_length = len(states)
        self.states = states
    def length(self):
        return self.path_length
    def start_state(self):
        return self.states[0]
    def state_at(self, index):
        return self.states[index]
    def prefix(self, index):
        return Path(self.states[:index+1])
    def suffix(self,index):
        return Path(self.states[index:])
    def get_states(self):
        return self.states
    def append(self,s):
        self.states.append(s)
        self.path_length += 1
    def to_str(self):
        return [s.to_str() for s in self.states]

class Model():
    def states(self):
        pass
    def rand_path(self, s,n):
        pass

class DTMC:
    def __init__(self, S, T, L):
        self.S = S
        self.N = len(self.S)
        self.T = T
        self.L = L
    def states(self):
        return self.S
    def num_states(self):
        return self.N
    def rand_path(self, s, n):
        #rpath = Path()
        rstates = []
        for i in range(n):
            s_ = []
            ps_ = []
            for key in self.T[s]:
                s_.append(key)
                ps_.append(self.T[s][key])
            next_state = choice(s_, replace=True, p=ps_)
            rstates.append(State(s, self.L[s]))
            s = next_state
        return Path(states=rstates)



