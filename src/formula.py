class Formula():
    def __init__(self, **kwargs):
        if 'subformulae' in kwargs.keys():
            self.subformulae = kwargs['subformulae']
        else:
            self.subformulae = []
        if 'path_var' in kwargs.keys():
            self.path_var = kwargs['path_var']
        else: 
            self.path_var = []
        if 'eps' in kwargs.keys():
            self.eps = kwargs['eps']
        else:
            self.eps = 5e-2
        if 'delta' in kwargs.keys():
            self.delta = kwargs['delta']
        else:
            self.delta = 1e-2
    def is_state_formula(self):
        raise AttributeError('{}.is_state_formula()'.format(self.__class__)+' is not defined')
    def get_subformulae(self):
        return self.subformulae
    def get_subformula_index(self, index):
        return self.subformulae[index]
    def get_path_vars(self):
        return self.path_var
    def set_eps(self, eps):
        self.eps = eps
    def set_delta(self, delta):
        self.delta = delta
    def get_eps(self):
        return self.eps
    def get_delta(self):
        return self.delta

class PathFormula(Formula):
    def __init__(self, **kwargs):
        super(PathFormula, self).__init__(**kwargs)
    def is_state_formula(self):
        return False

class StateFormula(PathFormula):
    def __init__(self, **kwargs):
        super(StateFormula, self).__init__(**kwargs)
    def is_state_formula(self):
        return True

class Top(Formula):
    def __init__(self, **kwargs):
        super(Top,self).__init__()

class Atomic(StateFormula):
    def __init__(self, **kwargs):
        self.label = kwargs['label']
        super(Atomic, self).__init__(**kwargs)
    def get_label(self):
        return self.label
class AtomicEq(StateFormula):
    def __init__(self, **kwargs):
        self.label1 = kwargs['label1']
        self.label2 = kwargs['label2']
        super(AtomicEq, self).__init__(**kwargs)
    def get_label1(self):
        return self.label1
    def get_label2(self):
        return self.label2
# Logical 
class Logic(Formula):
    def __init__(self, **kwargs):
        super(Logic, self).__init__(**kwargs)
    def is_state_formula(self):
        for formula in self.get_subformula():
            if not formula.is_state_formula():
                return False
        return True

class Not(Logic):
    def __init__(self, **kwargs):
        super(Not, self).__init__(**kwargs)

class And(Logic):
    def __init__(self, **kwargs):
        super(And, self).__init__(**kwargs)

# Temporal
class Next(PathFormula):
    def __init__(self, **kwargs):
        super(Next, self).__init__(**kwargs)

class Until(PathFormula):
    def __init__(self, **kwargs):
        super(Until, self).__init__(**kwargs)

# Quatification
class ForAll(StateFormula):
    def __init__(self, **kwargs):
        super(ForAll, self).__init__(**kwargs)

class Exists(StateFormula):
    def __init__(self, **kwargs):
        super(Exists, self).__init__(**kwargs)

# Probabilistic
class ProbFormula(Formula):
    def __init__(self, **kwargs):
        super(ProbFormula, self).__init__(**kwargs)
class Prob(ProbFormula):
    def __init__(self, **kwargs):
        self.p = kwargs.pop('p')
        super(Prob, self).__init__(**kwargs)
class ProbOp(ProbFormula):
    def __init__(self, **kwargs):
        super(ProbOp, self).__init__(**kwargs)
class ProbLT(ProbFormula):
    def __init__(self, **kwargs):
        super(ProbLT, self).__init__(**kwargs)
    def set_eps(self, eps):
        self.eps = eps
    def set_delta(self, delta):
        self.delta = delta
    def get_eps(self):
        return self.eps
    def get_delta(self):
        return self.delta

class ProbGT(ProbFormula):
    def __init(self, **kwargs):
        super(ProbGT, self).__init__(**kwargs)
class ProbEQ(ProbFormula):
    def __init(self, **kwargs):
        super(ProbEQ, self).__init__(**kwargs)






