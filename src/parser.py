import json
import sys
from model import *
from lark import Lark, Transformer, tree
from formula import *

class TreeToFormula(Transformer):
    def __init__(self):
        super(TreeToFormula, self).__init__()
    def prob(self, args):
        return Prob( **{'p' : args[0], 'subformulae': [], 'path_var': [] })
    def atomicf(self, args):
        return Atomic(**{'subformulae': [], 'path_var': [int(args[0].children[0])], 'label': str(args[1].children[0])})
    def truef(self, args):
        return Top()
    def notf(self, args):
        if isinstance(args[0], tree.Tree):
            subformula = args[0].children[0]
        else:
            subformula = args[0]
        return Not(**{'subformulae': [subformula], 'path_var': []})
    def nextf(self, args):
        if isinstance(args[0], tree.Tree):
            subformula = args[0].children[0]
        else:
            subformula = args[0]
        return Next(**{'subformulae': [subformula], 'path_var': []})
    def andf(self, args):
        subformulae = []
        if isinstance(args[0], tree.Tree):
            subformulae.append(args[0].children[0])
        else:
            subformulae.append(args[0])
        if isinstance(args[1], tree.Tree):
            subformulae.append(args[1].children[0])
        else:
            subformulae.append(args[1])
        return And(**{'subformulae':subformulae, 'path_var': []})
    def untilf(self, args):
        subformulae = []
        if isinstance(args[0], tree.Tree):
            subformulae.append(args[0].children[0])
        else:
            subformulae.append(args[0])
        if isinstance(args[1], tree.Tree):
            subformulae.append(args[1].children[0])
        else:
            subformulae.append(args[1])
        return Until(**{'subformulae':subformulae, 'path_var': []})
    def probop(self, args):
        path_var = []
        subformulae = []
        for arg in args:
            if isinstance(arg, tree.Tree):
                if arg.data == 'pvar':
                    path_var.append(int(arg.children[0][0]))
                if arg.data == 'formula':
                    subformulae.append(arg.children[0])
            else:
                subformulae.append(arg)
        return ProbOp(**{'subformulae':subformulae, 'path_var':path_var})
    def problt(self, args):
        path_var = []
        subformulae = [args[0], args[1]]
        eps = 5e-2
        delta = 1e-2
        return ProbLT(**{'subformulae': subformulae, 'path_var':path_var, 'eps':eps, 'delta':delta})
    def probgt(self, args):
        path_var = []
        eps = 5e-2
        delta = 1e-2
        subformulae = [args[1], args[0]]
        return ProbLT(**{'subformulae': subformulae, 'path_var':path_var, 'eps':eps, 'delta':delta})
    def probeq(self, args):
        eps = 5e-2
        delta = 1e-2
        path_var = []
        ltf = ProbLT(**{'subformulae': [args[0], args[1]], 'path_var':[], 'eps':eps/2, 'delta':delta})
        gtf = ProbLT(**{'subformulae': [args[1], args[0]], 'path_var':[], 'eps':eps/2, 'delta':delta})
        return And(**{'subformulae':[ltf, gtf], 'path_var':[]})
    def finalf(self, args):
        path_var = []
        eps = 5e-2
        delta = 1e-2
        subformulae = [Top(), args[0]]
        return Until(**{'subformulae': subformulae, 'path_var':[], 'eps':eps, 'delta': delta})
    def forall(self, args):
        if isinstance(args[0], tree.Tree):
            path_var = []
            subformulae = []
            eps = 5e-2
            delta = 1e-3
            #print(args)
            for arg in args:
                if isinstance(arg, tree.Tree):
                    if arg.data == 'pvar':
                        path_var.append(int(arg.children[0][0]))
                    else:
                        if arg.data == 'formula':
                            subformulae.append(arg.childen[0])
                elif isinstance(arg, Formula):
                    subformulae.append(arg)

            #print(path_var, subformulae)
        return ForAll(**{'subformulae': subformulae, 'path_var': path_var, 'eps':eps, 'delta':delta})




hpctls_grammar = r"""
    start: formula
    number: SIGNED_NUMBER -> pvar
    string: /[a-zA-Z_][a-zA-Z_0-9]*/ -> label
    formula: "(" formula ")"
        | "True" -> truef
        | "ForAll" "[" [number ("," number)*] "]" formula -> forall
        | "Exists" "[" [number ("," number)*] "]" formula -> exists
        | "Next"  formula -> nextf
        | "Not"  formula -> notf
        | "Final" formula -> finalf
        | formula "And" formula -> andf
        | formula "Until" formula -> untilf
        | "[" number "]" string-> atomicf
        | p "<" p -> problt
        | p ">" p -> probgt
        | p "=" p -> probeq
    p: "Pr" "[" [number ("," number)*] "]"  formula  -> probop
        | float
    float: SIGNED_FLOAT-> prob


    %import common.ESCAPED_STRING
    %import common.SIGNED_NUMBER
    %import common.SIGNED_FLOAT
    %import common.WS_INLINE
    %ignore WS_INLINE
"""

def json_to_dtmc(filename):
    S = []
    T = {}
    L = {}
    with open(filename) as json_file:
        data = json.load(json_file)
        for state in data['DTMC']['States']:
            s = state['name']
            S.append(s)
            T[s] = {}
            for s_ in state['Transitions']:
                T[s][s_] = state['Transitions'][s_]
            L[s] = state['AP']
    return data['DTMC']['start_state'],DTMC(S, T, L)  

def txt_to_formula(filename):
    f = open(filename)
    property = f.read()
    f.close()
    hpctls_parser = Lark(hpctls_grammar, parser='lalr')
    tree = hpctls_parser.parse(property)
    #print(tree.pretty())
    transformer = TreeToFormula()
    return transformer.transform(tree).children[0]
    
    



